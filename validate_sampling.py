# Script for validate the reprtition of groups
#
# Take a csv as argument and the invariants checked
# python validation.py path/to/your/data invariants

import pandas as pd
import numpy as np
import scipy.stats as st
import sys

# Prepare groups
sampling_df = pd.read_csv(sys.argv[1])
evaluate_df = sampling_df[['group_id', sys.argv[2]]]

df_A = evaluate_df[sampling_df.group_id % 2!= 0].sum()
df_B = evaluate_df[sampling_df.group_id % 2 == 0].sum()
df_A.group_id = 'A'
df_B.group_id = 'B'
print(df_A, '\n')
print(df_B, '\n')
# Number of visitors in groups A & B
N_A = df_A[1]
N_B = df_B[1]

# Standard deviation of binomial probability with 0.5 of success
SD = np.sqrt((0.5*0.5)/(N_A + N_B))
print('Standard deviation = ', SD)
# Margin of error (for an interval of confidence of 95%)
m = SD * st.norm.ppf(0.975)
print('Margine of error = ', m)
# Lower anf upper bounderies
d_min = 0.5 - m
d_max = 0.5 + m

# Probability for A & B
p_A = N_A/(N_A + N_B)
print('P_A = ', p_A)
p_B = N_B/(N_A + N_B)
print('P_B = ', p_B)

# Test (proba in the condidence interval)
is_pa_ok = True if d_min <= p_A <= d_max else False
is_pb_ok = True if d_min <= p_B <= d_max else False
print
print('Number of samples in group A is in the confidence interval: {0}\n\
Number of samples in group B is in the confidence interval: {1}'.format(is_pa_ok, is_pb_ok))
